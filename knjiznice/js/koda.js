
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var bolniki = [
	
	{
		"ime":"Mojca",
		"priimek":"Kovač",
		"datumRojstva":"1999-04-09",
		"datumInUra":"2018-05-26T13:00",
		"telesnaVisina":"160",
		"telesnaTeza":"55",
		"utrip":"88",
	},
	
	{
		"ime":"Gorazd",
		"priimek":"Rajec",
		"datumRojstva":"1991-04-10",
		"datumInUra":"2017-05-26T13:00",
		"telesnaVisina":"190",
		"telesnaTeza":"100",
		"utrip":"110",
	},
	
		{
		"ime":"David",
		"priimek":"Kos",
		"datumRojstva":"2004-08-10",
		"datumInUra":"2010-05-26",
		"telesnaVisina":"165",
		"telesnaTeza":"52",
		"utrip":"78",
	},
];
	

function generiraj(){
	for(var i = 0 ; i < 3; i++){
		generirajPodatke(i);
		console.log("test");
	}
}

function generirajPodatke(stPacienta) {
	var ehrId = "";
	sessionId = getSessionId();
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: bolniki[stPacienta].ime,
		            lastNames: bolniki[stPacienta].priimek,
		            dateOfBirth: bolniki[stPacienta].datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                
		                	
		                }
		                
		                 $("#preberiObstojeciEHR").append($('<option>',{
                            value: ehrId,
                            text: bolniki[stPacienta].ime
                        }));
                        
		            	 $("#preberiObstojeciVitalniZnak").append($('<option>',{
                            value: ehrId,
                            text: bolniki[stPacienta].ime
                        }));
		                
		                 $("#preberiEhrIdZaVitalneZnake").append($('<option>',{
                            value: ehrId,
                            text: bolniki[stPacienta].ime
                        }));
                        
                        $("#preberiObstojeceZdravilo").append($('<option>',{
                            value: ehrId,
                            text: bolniki[stPacienta].ime
                        }));
                        
                        $("#preberiEHRidZaZdravilo").append($('<option>',{
                            value: ehrId,
                            text: bolniki[stPacienta].ime
                        }));
                        
                       
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		            
		        });
		        
		        generirajVitalnePodatke(stPacienta,ehrId);
				
		    }
		});
		
	return ehrId;
}

function generirajVitalnePodatke(stPacienta, ehrId){
	
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": bolniki[stPacienta].datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": bolniki[stPacienta].telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": bolniki[stPacienta].telesnaTeza,
		    "vital_signs/pulse/any_event/rate|magnitude": bolniki[stPacienta].utrip
			
			
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    commiter: ''
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
		
		
}


function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                }
		            	
		            	 $("#preberiObstojeciVitalniZnak").append($('<option>',{
                            value: ehrId,
                            text: ime
                        }));

                         $("#preberiEhrIdZaVitalneZnake").append($('<option>',{
                            value: ehrId,
                            text: ime
                        }));
                        
                        $("#preberiObstojeceZdravilo").append($('<option>',{
                            value: ehrId,
                            text: ime
                        }));
                        
                        $("#preberiObstojeciEHR").append($('<option>',{
                            value: ehrId,
                            text: ime
                        }));
                        
		            },
		            
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-success fade-in'>Bolnik '" + party.firstNames + " " +
          party.lastNames + "', ki se je rodil '" + party.dateOfBirth +
          "'.</span>");
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}


function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var utrip = $("#dodajUtrip").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		    "vital_signs/pulse/any_event/rate|magnitude": utrip
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    commiter: ''
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


function preberiMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
	var tip = $("#preberiTipZaVitalneZnake").val();

	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#rezultatMeritveVitalnihZnakov").html("<br/><span>Pridobivanje " +
          "podatkov za <b>'" + tip + "'</b> bolnika <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
				if (tip == "utrip") {
					
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "pulse",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Utrip</th></tr>";
                    
                    			var utrip = [];
                    			var cas = [];
                    			
						        for (var i in res) {
						        	utrip[i] = res[i].pulse;
						        	cas[i] = res[i].time;
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].pulse +
                          " " + res[i].unit + "</td></tr>";
						        }
						        
						        var podtk = {
						        	
						        	x: cas,
						        	y: utrip
						        }
						        
						        graf(podtk);
						        
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					
					gugapi();
				} else if (tip == "telesna teža") {
					
					$.ajax({
					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Telesna teža</th></tr>";
                    
                    		var teza = [];
                    		var cajt = [];
						        for (var i in res) {
						        	
						        	teza[i] = res[i].weight;
						        	cajt[i] = res[i].time;
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].weight + " " 	+
                          res[i].unit + "</td></tr>";
						        }
						        
						        var podtk = {
						        	
						        	x: cajt,
						        	y: teza
						        }
						        
						        graf(podtk);
						        
						        results += "</table>";
						        $("#rezultatMeritveVitalnihZnakov").append(results);
					    	} else {
					    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
				}
	    	},
	    	error: function(err) {
	    		$("#preberiMeritveVitalnihZnakovSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
	
}


function graf(podatki) {
	
		var trace1 = {
			  x: [1, 2, 3, 4],
			  y: [10, 15, 13, 17],
			  type: 'scatter'
			};
	
	var trace2 = {
	  x: [1, 2, 3, 4],
	  y: [16, 5, 11, 9],
	  type: 'scatter'
	};
	
	var data = [podatki];
	
	console.log(podatki);
	Plotly.newPlot('graf', data);
	
}

function gugapi(){
	var url="https://www.googleapis.com/customsearch/v1?key=AIzaSyCugYjqvTTl_EfiixvWu_Yu4Ea1tSXb4LQ&cx=003111164528655770791:qfvsy1rem_q"
	var apikey = "AIzaSyCugYjqvTTl_EfiixvWu_Yu4Ea1tSXb4LQ";
	$.ajax(
		{
			url: url,
			type: "GET",
			data: {q: 'heart pulse', key:apikey},

			success:function(res){
				console.log(res.items[0].link);
				redirect(res.items[0].link);
			},
			error:function(err){
				console.log(err.responseText.userMessage);
			}
		}
	)
}

function redirect(link){
	var check = window.confirm("Do you want to redirect to an external site to learn more about Heart Rate?");
	
	if(check){
		window.open(link);
	}
}

$(document).ready(function() {

 
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
  });

	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});


	$('#preberiObstojeciVitalniZnak').change(function() {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("");
		var podatki = $(this).val().split("|");
		$("#dodajVitalnoEHR").val(podatki[0]);
		$("#dodajVitalnoDatumInUra").val(podatki[1]);
		$("#dodajVitalnoTelesnaVisina").val(podatki[2]);
		$("#dodajVitalnoTelesnaTeza").val(podatki[3]);
		$("#dodajUtrip").val(podatki[4]);
	});
	

	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#rezultatMeritveVitalnihZnakov").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});
	
});
